/* eslint-disable @next/next/no-img-element */
"use client";
import React from "react";
import { Button } from "@nextui-org/react";
import { FaArrowRight } from "react-icons/fa";

export default function Home() {
  return (
    <>
      {/* Kembangkan Ketrampilan */}
      <section>
        <style jsx>{`
          section {
            background-image: url("/Assets/BackgroundWebsite/home.png");
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
            padding: 0 1rem; /* px-10 in tailwind equivalent */
            width: 100%;
            height: 1000px;
          }

          @media (max-width: 600px) {
            section {
              background-image: url("/Assets/BackgroundApp/1.png");
              width: 1300px;
            }
            .mobile-pt {
              margin-top: 60rem;
            }
            .mobile-text {
              text-align: center;
            }
          }
        `}</style>

        <div>
          <nav className="flex justify-between items-center py-4 border-b">
            <div className="flex items-center space-x-4">
              <div className="flex flex-row justify-center items-center space-x-7">
                <div className="flex flex-row items-center">
                  <div className="text-5xl font-bold text-blue-400">Bebras</div>
                  <div className="text-5xl font-bold text-red-400">
                    Indonesia
                  </div>
                  <img
                    alt="Bebras Indonesia"
                    className="h-16 px-7"
                    src="/Assets/Images/logo.png"
                  />
                </div>
              </div>
              <div className="flex">
                <a
                  className="text-gray-600 text-xl font-bold hover:text-gray-800 px-7"
                  href="#"
                >
                  Tentang Bebras
                </a>
                <a
                  className="text-gray-600 text-xl font-bold hover:text-gray-800 px-7"
                  href="#"
                >
                  Gerakan Bebras
                </a>
                <a
                  className="text-gray-600 text-xl font-bold hover:text-gray-800 px-7"
                  href="#"
                >
                  Kegiatan
                </a>
                <a
                  className="text-gray-600 text-xl font-bold hover:text-gray-800 px-7"
                  href="#"
                >
                  Kontak
                </a>
              </div>
            </div>
            <Button className="bg-[#05ACEE] text-white px-7 py-3 rounded-full flex flex-row space-x-4">
              <div>Download aplikasinya</div>
              <FaArrowRight />
            </Button>
          </nav>
        </div>
        <div className="text-8xl font-bold text-gray-800 mb-4 mt-11 mobile-pt mobile-text">
          Kembangkan
        </div>
        <div className="text-8xl font-bold text-gray-800 mb-4 mobile-text">
          Keterampilan
        </div>
        <div className="text-8xl font-bold text-gray-800 mb-4 mobile-text">
          Berpikir si kecil
        </div>
        <div className="py-10 mobile-text">
          <div className="text-4xl mb-2">
            Tingkatkan kompetensi sosial dan emosional si
          </div>
          <div className="text-4xl mb-7 mobile-text">
            kecil dan siapkan ia untuk mengarungi dunia.
          </div>
          <div className="py-10 mobile-text">
            <Button className="bg-[#FDD34C] text-black px-7 py-3 flex flex-row space-x-4 rounded-full">
              <div>Download aplikasi Bebras Pandai</div>
              <FaArrowRight />
            </Button>
          </div>
          <div className="pt-20 pb-5">
            <div className="text-2xl mb-3">
              Viverra vitae nisl nibh et aliquam, enim. Tempor sit, nisl viverra
            </div>
            <div className="text-2xl mb-3">
              orci consequat tempus sociiseneque faucibus.
            </div>
          </div>
          <div className="flex flex-row items-center">
            <div className="flex w-20 h-20 rounded-full bg-fuchsia-300 "></div>
            <div className="flex flex-col pl-3">
              <div className="text-xl text-gray-400">Rendika Herfian</div>
              <div className="text-xl text-gray-400">zerowaste.com</div>
            </div>
          </div>
        </div>
      </section>

      {/* Tentang Bebras */}
      <section>
        <style jsx>{`
          section {
            background-image: url("/Assets/BackgroundWebsite/tentangBebras.png");
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
            padding: 0 1rem; /* px-10 in tailwind equivalent */
            width: 100%;
            height: 1000px;
          }

          @media (max-width: 600px) {
            section {
              width: 1300px;
            }
            .mobile-pt {
              margin-top: 60rem;
            }
            .mobile-text {
              text-align: center;
            }
          }
        `}</style>
        <div className="flex flex-col items-center p-11 mobile-pt">
          <div className="text-2xl text-orange-600 mt-9 font-bold">
            TENTANG BEBRAS
          </div>
          <div className="text-6xl font-bold mt-3">Didukung oleh Google</div>
          <div className="text-2xl mt-9">
            Kegiatan yang dilaksanakan oleh Bebras Indonesia untuk mengajarkan
            Computational
          </div>
          <div className="text-2xl mt-3">
            Thinking merupakan cabang dari organisasi Bebras Internasional.
          </div>
        </div>
      </section>

      {/* Computational Thinking */}
      <section>
        <style jsx>{`
          section {
            background-image: url("/Assets/BackgroundWebsite/computationalThinking.png");
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
            padding: 0 1rem; /* px-10 in tailwind equivalent */
            width: 100%;
            height: 1000px;
          }

          @media (max-width: 600px) {
            section {
              width: 1300px;
              margin-top: 10rem;
              text-align: center;
            }
          }
        `}</style>
        <div className="flex flex-row h-full mobile-pt">
          <div className="flex flex-1"></div>
          <div className="flex flex-1 flex-col justify-center mx-24">
            <div className="text-2xl text-orange-600 mt-9 font-bold">
              COMPUTATIONAL THINKING
            </div>
            <div className="text-6xl font-bold mt-3">
              Berfikir runut itu mudah
            </div>
            <div className="text-2xl mt-9">
              Kenalkan cara penyelesaian masalah secara komputasional kepada
              anak
            </div>
            <div className="text-2xl mt-3">
              sedari dini agar ia terbiasa mencari solusi yang efektif dan
              efisien.
            </div>
            <div className="grid grid-cols-2 gap-8 mt-20">
              <div className="flex flex-col">
                <img
                  src="Assets/icon/truck.png"
                  alt="truck"
                  className="w-24 h-24 mb-3"
                />
                <div className="text-2xl font-semibold text-gray-800 mb-2">
                  Identifikasi Masalah
                </div>
                <div className="text-gray-600 mb-4 text-lg">
                  Si kecil akan belajar untuk menganalisis masalah besar untuk
                  dipecahkan menjadi lebih kecil sehingga lebih mudah untuk
                  diselesaikan.
                </div>
              </div>
              <div className="flex flex-col">
                <img
                  src="Assets/icon/trophy.png"
                  alt="trekking"
                  className="w-24 h-24 mb-3"
                />
                <div className="text-2xl font-semibold text-gray-800 mb-2">
                  Pengenalan Pola
                </div>
                <div className="text-gray-600 mb-4 text-lg">
                  Si kecil akan belajar mengidentifikasi pola atau persamaan
                  tertentu yang terdapat di suatu masalah.
                </div>
              </div>
              <div className="flex flex-col">
                <img
                  src="Assets/icon/trekking.png"
                  alt="trekking"
                  className="w-24 h-24 mb-3"
                />
                <div className="text-2xl font-semibold text-gray-800 mb-2">
                  Pemecahan Masalah
                </div>
                <div className="text-gray-600 mb-4 text-lg">
                  Si kecil akan belajar untuk mengidentifikasi informasi dan
                  mensortir relevansi informasi yang tersedia untuk memecahkan
                  masalah yang disajikan.
                </div>
              </div>
              <div className="flex flex-col">
                <img
                  src="Assets/icon/piggy-bank.png"
                  alt="trekking"
                  className="w-24 h-24 mb-3"
                />
                <div className="text-2xl font-semibold text-gray-800 mb-2">
                  Pengembangan Solusi
                </div>
                <div className="text-gray-600 mb-4 text-lg">
                  Si kecil akan belajar mengembangkan sistem, membuat daftar
                  petunjuk dan langkah-langkah pemecahan masalah secara efektif
                  dan efisien.
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      {/* Aplikasi Bebras Pandai */}
      <section>
        <style jsx>{`
          section {
            background-image: url("/Assets/BackgroundWebsite/aplikasiBebrasPandai.png");
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
            padding: 0 1rem; /* px-10 in tailwind equivalent */
            width: 100%;
            height: 1000px;
          }

          @media (max-width: 600px) {
            section {
              width: 1300px;
            }
            .mobile-pt {
              margin-top: 10rem;
            }
            .mobile-text {
              text-align: center;
            }
          }
        `}</style>
        <div className="flex flex-row h-full mobile-pt">
          <div className="flex flex-1 flex-col justify-center mx-24">
            <div className="text-2xl text-orange-600 mt-9 font-bold">
              APLIKASI BEBRAS PANDAI
            </div>
            <div className="text-6xl font-bold mt-3">Bukan sembarang game</div>
            <div className="text-2xl mt-9">
              Belajar tidak harus selalu menjadi momok yang menakutkan. Bebras
            </div>
            <div className="text-2xl mt-3">
              Indonesia telah pendidikan berstandar internasional.
            </div>
            <div className="grid grid-cols-2 gap-8 mt-20">
              <div className="flex flex-col">
                <img
                  src="Assets/icon/tournament.png"
                  alt="tournament"
                  className="w-24 h-24 mb-3"
                />
                <div className="text-2xl font-semibold text-gray-800 mb-2">
                  Mudah untuk dicerna
                </div>
                <div className="text-gray-600 mb-4 text-lg">
                  Soal-soal di Bebras App dapat dijawab tanpa perlu belajar
                  informatika terlebih dahulu, namun tetap mengajarkan konsep
                  tertentu dalam informatika dan computational thinking.
                </div>
              </div>
              <div className="flex flex-col">
                <img
                  src="Assets/icon/thumbs-up.png"
                  alt="thumbs-up"
                  className="w-24 h-24 mb-3"
                />
                <div className="text-2xl font-semibold text-gray-800 mb-2">
                  Visual yang menarik
                </div>
                <div className="text-gray-600 mb-4 text-lg">
                  Disajikan dalam bentuk uraian persoalan yang dilengkapi dengan
                  gambar yang menarik, sehingga si kecil dapat lebih mudah
                  memahami soal.
                </div>
              </div>
              <div className="flex flex-col">
                <img
                  src="Assets/icon/transition-top.png"
                  alt="transition-top"
                  className="w-24 h-24 mb-3"
                />
                <div className="text-2xl font-semibold text-gray-800 mb-2">
                  Kompetisi Bebras
                </div>
                <div className="text-gray-600 mb-4 text-lg">
                  Setiap tahun, Bebras App menggelar acara skala besar bernama
                  Bebras Challenge dimana semua pengguna dapat berkompetisi
                  dengan satu sama lain.
                </div>
              </div>
              <div className="flex flex-col">
                <img
                  src="Assets/icon/perspective-view.png"
                  alt="perspective-view"
                  className="w-24 h-24 mb-3"
                />
                <div className="text-2xl font-semibold text-gray-800 mb-2">
                  Tantangan mingguan
                </div>
                <div className="text-gray-600 mb-4 text-lg">
                  Untuk membangkitkan semangat kompetisi, Bebras App menyajikan
                  tantangan mingguan yang beragam dan tidak membosankan.
                </div>
              </div>
            </div>
          </div>
          <div className="flex flex-1"></div>
        </div>
      </section>

      {/* Testimonial */}
      <section>
        <style jsx>{`
          section {
            background-image: url("/Assets/BackgroundWebsite/testimonial.png");
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
            padding: 0 1rem; /* px-10 in tailwind equivalent */
            width: 100%;
            height: 1000px;
          }

          @media (max-width: 600px) {
            section {
              width: 1300px;
              margin-top: 10rem;
              text-align: center;
            }
          }
        `}</style>
        <div className="flex flex-col mobile-pt mobile-text">
          <div className="text-2xl text-orange-300 mt-9 font-bold">
            TESTIMONIAL
          </div>
          <div className="text-6xl text-white font-bold mt-3">
            Apa kata mereka?
          </div>
          <div className="text-2xl text-white mt-9">
            Bebras Pandai telah digunakan oleh 200.000+ pengguna dari penjuru
            Indonesia.
          </div>
          <div className=" mt-28">
            <Button className="bg-[#FDD34C] text-black px-7 py-3 flex flex-row space-x-4 rounded-full">
              <div>Download aplikasi Bebras Pandai</div>
              <FaArrowRight />
            </Button>
          </div>
          <div className="flex flex-row mt-20 h-fit">
            <div className="bg-white m-3 rounded-xl shadow">
              <div className="text-xl text-gray-600 mb-2 p-5">
                <img
                  src="Assets/icon/thunderstorm.png"
                  alt="thunderstorm"
                  className="w-24 h-24 mb-3"
                />
                <div className=" mb-12">
                  Placeat necessitatibus est, qui aut minus, quia dolor possimus
                  soluta aut sint. Sint officia Quasi reiciendis non molestiae
                  cum, quae voluptatem sequi numquam. Temporibus aut commodi
                  odio eiusmod.
                </div>
                <div className="flex flex-row items-center">
                  <div className="flex w-20 h-20 rounded-full bg-fuchsia-300 "></div>
                  <div className="flex flex-col pl-3">
                    <div className="text-lg text-gray-500">Jane Cooper</div>
                  </div>
                </div>
              </div>
            </div>
            <div className="bg-white m-3 rounded-lg shadow">
              <div className="text-xl text-gray-600 mb-2 p-5">
                <img
                  src="Assets/icon/three-stars.png"
                  alt="three-stars"
                  className="w-24 h-24 mb-3"
                />
                <div className=" mb-12">
                  Voluptatum ut sit amet inventore debitis eius. Providentia
                  Porro, omnis, eum, ullam, quam. Molestiae et placeat est. Est
                  quia ut et similacrum. Est enim est nihil, non, in eiusmod
                  tempor.
                </div>
                <div className="flex flex-row items-center">
                  <div className="flex w-20 h-20 rounded-full bg-fuchsia-300 "></div>
                  <div className="flex flex-col pl-3">
                    <div className="text-lg text-gray-500">Ralph Edwards</div>
                  </div>
                </div>
              </div>
            </div>
            <div className="bg-white m-3 rounded-lg shadow">
              <div className="text-xl text-gray-600 mb-2 p-5">
                <img
                  src="Assets/icon/tower.png"
                  alt="tower"
                  className="w-24 h-24 mb-3"
                />
                <div className=" mb-12">
                  Viverra lacus suspendisse elit, sapien. Viverra, vero etiam
                  tempus adipiscing non. Vivamus suscipit sem neque tempus.
                  Etiam consequat semper libero! Duis cum sociis natoque
                  penatibus et lorem interdum dolor, nisi aliquet, condimentum.
                </div>
                <div className="flex flex-row items-center">
                  <div className="flex w-20 h-20 rounded-full bg-fuchsia-300 "></div>
                  <div className="flex flex-col pl-3">
                    <div className="text-lg text-gray-500">Courtney Henry</div>
                  </div>
                </div>
              </div>
            </div>
            <div className="bg-white m-3 rounded-lg shadow">
              <div className="text-xl text-gray-600 mb-2 p-5">
                <img
                  src="Assets/icon/timer.png"
                  alt="timer"
                  className="w-24 h-24 mb-3"
                />
                <div className=" mb-12">
                  Hendrerit augue eu nisl nunc. Nisi velit sed dolorum. Est
                  amet. Egestas amet amet. Tempus aliquam amet nunc fugit. Sed
                  hendrerit augue eu nisl nunc. Nisi velit sed dolorum. Est
                  amet. Egestas amet amet. Tempus aliquam.
                </div>
                <div className="flex flex-row items-center">
                  <div className="flex w-20 h-20 rounded-full bg-fuchsia-300 "></div>
                  <div className="flex flex-col pl-3">
                    <div className="text-lg text-gray-500">
                      Cameron Williamson
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      {/* Kegiatan Gerakan Pandai */}
      <section>
        <style jsx>{`
          section {
            background-image: url("/Assets/BackgroundWebsite/kegiatanGerakanPandai.png");
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
            padding: 0 1rem; /* px-10 in tailwind equivalent */
            width: 100%;
            height: 1050px;
          }

          @media (max-width: 600px) {
            section {
              width: 1300px;
              margin-top: 10rem;
              text-align: center;
            }
            .mobile-pt {
              margin-top: 10rem;
            }
            .mobile-text {
              text-align: center;
            }
          }
        `}</style>
        <div className="flex flex-row h-full">
          <div className="flex flex-1"></div>
          <div className="flex flex-1">
            <div className="flex flex-col justify-center p-11">
              <div className="text-2xl text-orange-600 mt-9 font-bold">
                KEGIATAN GERAKAN PANDAI
              </div>
              <div className="text-6xl font-bold mt-3">
                Bebras Pandai hadir untuk anak Indonesia.
              </div>
              <div className="text-2xl text-white mt-9">
                Gerakan Pandai telah diselenggarakan untuk 2 juta siswa, melalui
                22.000 guru berbagai mata pelajaran di 22 daerah di Indonesia.
              </div>
              <div>
                <Button
                  className="border-gray-300 text-orange-800 text-lg font-bold"
                  variant="outline"
                >
                  <div className="flex flex-row items-center">
                    <div className="mr-3">Ikuti kegiatan Bebras lainnya</div>
                    <FaArrowRight />
                  </div>
                </Button>
              </div>
            </div>
          </div>
        </div>
      </section>

      {/* Kita Semua Ingin ..... */}
      <section>
        <style jsx>{`
          section {
            background-image: url("/Assets/BackgroundWebsite/kitaSemua.png");
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
            padding: 0 1rem; /* px-10 in tailwind equivalent */
            width: 100%;
            height: 1300px;
          }

          @media (max-width: 600px) {
            section {
              width: 1300px;
              margin-top: 10rem;
              text-align: center;
            }
          }
        `}</style>
        <div className="flex flex-row h-full">
          <div className="flex flex-1">
            <div className="flex flex-col p-14">
              <div className="text-2xl text-orange-300 mt-9 mr-11 font-bold">
                KITA SEMUA INGIN YANG TERBAIK UNTUK SI BUAH HATI
              </div>
              <div className="text-7xl text-white font-bold mt-3">
                Ingin si kecil bisa berpikir jernih tentang masalahnya?
              </div>
            </div>
          </div>
          <div className="flex flex-21flex-col pt-32 pr-11 items-center">
            <div>
              <div>
                <Button className="bg-[#FDD34C] text-black px-12 py-3 flex flex-row space-x-4 rounded-full">
                  <div className=" font-bold text-2xl mr-5">Mulai di sini</div>
                  <FaArrowRight />
                </Button>
              </div>
            </div>
            <div className="text-2xl mt-5 text-center">
              Gabung dengan 20.000+ anak lainnya!
            </div>
          </div>
        </div>
      </section>

      {/* Footer */}
      <div>
        <nav className="flex justify-between items-center py-4 border-b px-14">
          <div className="flex flex-row justify-center items-center space-x-14">
            <div className="flex flex-row items-center">
              <div className="text-3xl font-bold text-blue-400">Bebras</div>
              <div className="text-3xl font-bold text-red-400">Indonesia</div>
              <img
                alt="Bebras Indonesia"
                className="h-10 px-7"
                src="/Assets/Images/logo.png"
              />
            </div>
            <div>@ Copyrights 2024</div>
            <a href="/">Beranda</a>
            <a href="/">Privacy</a>
            <a href="/">Kontak</a>
            <a href="">
              <img
                src="Assets/icon/youtube.png"
                alt="youtube"
                className="w-10 h-10"
              />
            </a>
            <a href="/">
              <img
                src="Assets/icon/facebook.png"
                alt="facebook"
                className="w-10 h-10"
              />
            </a>
            <a href="/">
              <img
                src="Assets/icon/twitter.png"
                alt="twitter"
                className="w-10 h-10"
              />
            </a>
            <a href="/">
              <img
                src="Assets/icon/instagram.png"
                alt="instagram"
                className="w-10 h-10"
              />
            </a>
            <a href="/">
              <img
                src="Assets/icon/linkedin.png"
                alt="linkedin"
                className="w-10 h-10"
              />
            </a>
            <a href="/">
              <img
                src="Assets/icon/GooglePlay.png"
                alt="linkedin"
                className="w-15 h-10"
              />
            </a>
            <a href="/">
              <img
                src="Assets/icon/AppStore.png"
                alt="linkedin"
                className="w-15 h-10"
              />
            </a>
          </div>
        </nav>
      </div>
    </>
  );
}
